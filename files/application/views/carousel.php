
<!-- =========================
     SECTION 0 - CAROUSEL
============================== -->
<section class="carsec" id="carsec">

	<div class="color-overlay">
		<div class="row">			
			<div class="col-md-12 col-sm-12">	
				<div id="carouselControls" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/car1.jpg" alt="Jegrun slide 1">
						</div>
						<div class="item">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/car2.jpg" alt="Jegrun slide 2">
						</div>
						<div class="item">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/car3.jpg" alt="Jegrun slide 3">
						</div>
						</div>
						<a class="left carousel-control" href="#carouselControls" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carouselControls" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
				</div>
			</div>
	  	</div><!-- /END - CAROUSEL -->
	</div>
</section>