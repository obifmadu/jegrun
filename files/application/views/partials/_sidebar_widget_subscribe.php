<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!--Widget: Tags-->
<div class="sidebar-widget">
    <div class="widget-head">
        <h4 class="title"><?php echo html_escape($widget->title); ?></h4>
    </div>
    <div class="widget-body">
        <div>
            <?php echo form_open('add-to-newsletter'); ?>
            <div class="newsletter">
                <div class="left">
                    <input type="email" name="email" id="newsletter_email_footer" maxlength="199" placeholder="<?php echo trans("placeholder_email"); ?>" required <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?>>
                </div>
                <div class="right">
                    <button type="submit" id="btn_subscribe_footer" class="newsletter-button"><?php echo trans("subscribe"); ?></button>
                </div>
            </div>
            <p id="newsletter">
                <?php
                if ($this->session->flashdata('news_error')):
                    echo '<span class="text-danger">' . $this->session->flashdata('news_error') . '</span>';
                endif;
                if ($this->session->flashdata('news_success')):
                    echo '<span class="text-success">' . $this->session->flashdata('news_success') . '</span>';
                endif;
                ?>
            </p>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
