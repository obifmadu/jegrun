<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Section: wrapper -->
<section id="wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12 page-login">
                <div class="row center-box">
                    <div class="col-sm-12 col-xs-12">
                        <div class="login-box">

                            <!-- form start -->
                        	<?php echo form_open("register-post", ['id' => 'form_validate', 'class' => 'validate_terms']); ?>

                            <!-- include message block -->
                            <?php $this->load->view('partials/_messages'); ?>

                            <h1 class="auth-title font-1"><?php echo trans("register"); ?></h1>
                                <div class="form-group">
                					<label for="username">Username</label>
                                    <input type="text" name="username" class="form-control form-input"
                                           placeholder="<?php echo trans("placeholder_username"); ?>"
                                           value="<?php echo old("username"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                			    </div>

                        		<div class="form-group">
                					<label for="first-name">First Name</label>
                				    <input type="text" id="first-name" class="form-control form-input"
                                    placeholder="<?php echo trans("placeholder_first-name"); ?>"
                                    value="<?php echo old("first-name"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="first-name" />
                                </div>

                                <div class="form-group">
                    				<label for="last-name">Last Name</label>
                    				<input type="text" id="last-name" class="form-control form-input"
                                    placeholder="<?php echo trans("placeholder_last-name"); ?>"
                                    value="<?php echo old("last-name"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="last-name" />
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" class="form-control form-input"
                                           placeholder="<?php echo trans("placeholder_email"); ?>"
                                           value="<?php echo old("email"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                                </div>

                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <ul>
                						<li><input type="radio" required id="gender" name="gender" value="male" />
                							<label>Male</label>
                						</li>
                						<li><input type="radio" required id="gender" name="gender" value="female" />
                							<label>Female</label>
                						</li>
                					</ul>
                                </div>

                                <div class="form-group">
                                    <label for="mobile-number">Mobile Number</label>
                                    <input type="tel" id="mobile-number" class="form-control form-input"
                                    placeholder="<?php echo trans("placeholder_mobile-number"); ?>"
                                    value="<?php echo old("mobile-number"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="mobile-number" />

                                </div>

                                <div class="form-group">
                                    <label for="age-range">Age Range</label>
                                    <select size="1" name="age-range" id="age-range" class="form-control form-input" required>
                						<option value="children">0 - 12yrs</option>
                						<option value="teenagers">13 - 17yrs</option>
                						<option value="young-adults">18 - 25yrs</option>
                						<option value="adults">26 - 40yrs</option>
                						<option value="late-adults">41 - 59yrs</option>
                						<option value="elders">60 - Up</option>
                					</select>
                                </div>

                                <div class="form-group">
                                    <label for="country-birth">Country of Origin</label>
                                    <select id="country-birth" class="form-control form-input"
                                    placeholder="<?php echo trans("placeholder_country-birth"); ?>"
                                    value="<?php echo old("country-birth"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?>required name="country-birth">
                        						<option>Choose Your Country</option>
                        						<option value="AF">Afghanistan</option>
                        						<option value="AL">Albania</option>
                        						<option value="DZ">Algeria</option>
                        						<option value="AS">American Samoa</option>
                        						<option value="AD">Andorra</option>
                        						<option value="AO">Angola</option>
                        						<option value="AI">Anguilla</option>
                        						<option value="AQ">Antarctica</option>
                        						<option value="AG">Antigua and Barbuda</option>
                        						<option value="AR">Argentina</option>
                        						<option value="AM">Armenia</option>
                        						<option value="AW">Aruba</option>
                        						<option value="AU">Australia</option>
                        						<option value="AT">Austria</option>
                        						<option value="AZ">Azerbaijan</option>
                        						<option value="BS">Bahamas</option>
                        						<option value="BH">Bahrain</option>
                        						<option value="BD">Bangladesh</option>
                        						<option value="BB">Barbados</option>
                        						<option value="BY">Belarus</option>
                        						<option value="BE">Belgium</option>
                        						<option value="BZ">Belize</option>
                        						<option value="BJ">Benin</option>
                        						<option value="BM">Bermuda</option>
                        						<option value="BT">Bhutan</option>
                        						<option value="BO">Bolivia</option>
                        						<option value="BA">Bosnia and Herzegowina</option>
                        						<option value="BW">Botswana</option>
                        						<option value="BV">Bouvet Island</option>
                        						<option value="BR">Brazil</option>
                        						<option value="IO">British Indian Ocean Territory</option>
                        						<option value="BN">Brunei Darussalam</option>
                        						<option value="BG">Bulgaria</option>
                        						<option value="BF">Burkina Faso</option>
                        						<option value="BI">Burundi</option>
                        						<option value="KH">Cambodia</option>
                        						<option value="CM">Cameroon</option>
                        						<option value="CA">Canada</option>
                        						<option value="CV">Cape Verde</option>
                        						<option value="KY">Cayman Islands</option>
                        						<option value="CF">Central African Republic</option>
                        						<option value="TD">Chad</option>
                        						<option value="CL">Chile</option>
                        						<option value="CN">China</option>
                        						<option value="CX">Christmas Island</option>
                        						<option value="CC">Cocos (Keeling) Islands</option>
                        						<option value="CO">Colombia</option>
                        						<option value="KM">Comoros</option>
                        						<option value="CG">Congo</option>
                        						<option value="CD">Congo, the Democratic Republic of the</option>
                        						<option value="CK">Cook Islands</option>
                        						<option value="CR">Costa Rica</option>
                        						<option value="CI">Cote d'Ivoire</option>
                        						<option value="HR">Croatia (Hrvatska)</option>
                        						<option value="CU">Cuba</option>
                        						<option value="CY">Cyprus</option>
                        						<option value="CZ">Czech Republic</option>
                        						<option value="DK">Denmark</option>
                        						<option value="DJ">Djibouti</option>
                        						<option value="DM">Dominica</option>
                        						<option value="DO">Dominican Republic</option>
                        						<option value="TP">East Timor</option>
                        						<option value="EC">Ecuador</option>
                        						<option value="EG">Egypt</option>
                        						<option value="SV">El Salvador</option>
                        						<option value="GQ">Equatorial Guinea</option>
                        						<option value="ER">Eritrea</option>
                        						<option value="EE">Estonia</option>
                        						<option value="ET">Ethiopia</option>
                        						<option value="FK">Falkland Islands (Malvinas)</option>
                        						<option value="FO">Faroe Islands</option>
                        						<option value="FJ">Fiji</option>
                        						<option value="FI">Finland</option>
                        						<option value="FR">France</option>
                        						<option value="FX">France, Metropolitan</option>
                        						<option value="GF">French Guiana</option>
                        						<option value="PF">French Polynesia</option>
                        						<option value="TF">French Southern Territories</option>
                        						<option value="GA">Gabon</option>
                        						<option value="GM">Gambia</option>
                        						<option value="GE">Georgia</option>
                        						<option value="DE">Germany</option>
                        						<option value="GH">Ghana</option>
                        						<option value="GI">Gibraltar</option>
                        						<option value="GR">Greece</option>
                        						<option value="GL">Greenland</option>
                        						<option value="GD">Grenada</option>
                        						<option value="GP">Guadeloupe</option>
                        						<option value="GU">Guam</option>
                        						<option value="GT">Guatemala</option>
                        						<option value="GN">Guinea</option>
                        						<option value="GW">Guinea-Bissau</option>
                        						<option value="GY">Guyana</option>
                        						<option value="HT">Haiti</option>
                        						<option value="HM">Heard and Mc Donald Islands</option>
                        						<option value="VA">Holy See (Vatican City State)</option>
                        						<option value="HN">Honduras</option>
                        						<option value="HK">Hong Kong</option>
                        						<option value="HU">Hungary</option>
                        						<option value="IS">Iceland</option>
                        						<option value="IN">India</option>
                        						<option value="ID">Indonesia</option>
                        						<option value="IR">Iran (Islamic Republic of)</option>
                        						<option value="IQ">Iraq</option>
                        						<option value="IE">Ireland</option>
                        						<option value="IL">Israel</option>
                        						<option value="IT">Italy</option>
                        						<option value="JM">Jamaica</option>
                        						<option value="JP">Japan</option>
                        						<option value="JO">Jordan</option>
                        						<option value="KZ">Kazakhstan</option>
                        						<option value="KE">Kenya</option>
                        						<option value="KI">Kiribati</option>
                        						<option value="KP">Korea, Democratic People's Republic of</option>
                        						<option value="KR">Korea, Republic of</option>
                        						<option value="KW">Kuwait</option>
                        						<option value="KG">Kyrgyzstan</option>
                        						<option value="LA">Lao People's Democratic Republic</option>
                        						<option value="LV">Latvia</option>
                        						<option value="LB">Lebanon</option>
                        						<option value="LS">Lesotho</option>
                        						<option value="LR">Liberia</option>
                        						<option value="LY">Libyan Arab Jamahiriya</option>
                        						<option value="LI">Liechtenstein</option>
                        						<option value="LT">Lithuania</option>
                        						<option value="LU">Luxembourg</option>
                        						<option value="MO">Macau</option>
                        						<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                        						<option value="MG">Madagascar</option>
                        						<option value="MW">Malawi</option>
                        						<option value="MY">Malaysia</option>
                        						<option value="MV">Maldives</option>
                        						<option value="ML">Mali</option>
                        						<option value="MT">Malta</option>
                        						<option value="MH">Marshall Islands</option>
                        						<option value="MQ">Martinique</option>
                        						<option value="MR">Mauritania</option>
                        						<option value="MU">Mauritius</option>
                        						<option value="YT">Mayotte</option>
                        						<option value="MX">Mexico</option>
                        						<option value="FM">Micronesia, Federated States of</option>
                        						<option value="MD">Moldova, Republic of</option>
                        						<option value="MC">Monaco</option>
                        						<option value="MN">Mongolia</option>
                        						<option value="MS">Montserrat</option>
                        						<option value="MA">Morocco</option>
                        						<option value="MZ">Mozambique</option>
                        						<option value="MM">Myanmar</option>
                        						<option value="NA">Namibia</option>
                        						<option value="NR">Nauru</option>
                        						<option value="NP">Nepal</option>
                        						<option value="NL">Netherlands</option>
                        						<option value="AN">Netherlands Antilles</option>
                        						<option value="NC">New Caledonia</option>
                        						<option value="NZ">New Zealand</option>
                        						<option value="NI">Nicaragua</option>
                        						<option value="NE">Niger</option>
                        						<option value="NG">Nigeria</option>
                        						<option value="NU">Niue</option>
                        						<option value="NF">Norfolk Island</option>
                        						<option value="MP">Northern Mariana Islands</option>
                        						<option value="NO">Norway</option>
                        						<option value="OM">Oman</option>
                        						<option value="PK">Pakistan</option>
                        						<option value="PW">Palau</option>
                        						<option value="PA">Panama</option>
                        						<option value="PG">Papua New Guinea</option>
                        						<option value="PY">Paraguay</option>
                        						<option value="PE">Peru</option>
                        						<option value="PH">Philippines</option>
                        						<option value="PN">Pitcairn</option>
                        						<option value="PL">Poland</option>
                        						<option value="PT">Portugal</option>
                        						<option value="PR">Puerto Rico</option>
                        						<option value="QA">Qatar</option>
                        						<option value="RE">Reunion</option>
                        						<option value="RO">Romania</option>
                        						<option value="RU">Russian Federation</option>
                        						<option value="RW">Rwanda</option>
                        						<option value="KN">Saint Kitts and Nevis</option>
                        						<option value="LC">Saint LUCIA</option>
                        						<option value="VC">Saint Vincent and the Grenadines</option>
                        						<option value="WS">Samoa</option>
                        						<option value="SM">San Marino</option>
                        						<option value="ST">Sao Tome and Principe</option>
                        						<option value="SA">Saudi Arabia</option>
                        						<option value="SN">Senegal</option>
                        						<option value="SC">Seychelles</option>
                        						<option value="SL">Sierra Leone</option>
                        						<option value="SG">Singapore</option>
                        						<option value="SK">Slovakia (Slovak Republic)</option>
                        						<option value="SI">Slovenia</option>
                        						<option value="SB">Solomon Islands</option>
                        						<option value="SO">Somalia</option>
                        						<option value="ZA">South Africa</option>
                        						<option value="GS">South Georgia and the South Sandwich Islands</option>
                        						<option value="ES">Spain</option>
                        						<option value="LK">Sri Lanka</option>
                        						<option value="SH">St. Helena</option>
                        						<option value="PM">St. Pierre and Miquelon</option>
                        						<option value="SD">Sudan</option>
                        						<option value="SR">Suriname</option>
                        						<option value="SJ">Svalbard and Jan Mayen Islands</option>
                        						<option value="SZ">Swaziland</option>
                        						<option value="SE">Sweden</option>
                        						<option value="CH">Switzerland</option>
                        						<option value="SY">Syrian Arab Republic</option>
                        						<option value="TW">Taiwan, Province of China</option>
                        						<option value="TJ">Tajikistan</option>
                        						<option value="TZ">Tanzania, United Republic of</option>
                        						<option value="TH">Thailand</option>
                        						<option value="TG">Togo</option>
                        						<option value="TK">Tokelau</option>
                        						<option value="TO">Tonga</option>
                        						<option value="TT">Trinidad and Tobago</option>
                        						<option value="TN">Tunisia</option>
                        						<option value="TR">Turkey</option>
                        						<option value="TM">Turkmenistan</option>
                        						<option value="TC">Turks and Caicos Islands</option>
                        						<option value="TV">Tuvalu</option>
                        						<option value="UG">Uganda</option>
                        						<option value="UA">Ukraine</option>
                        						<option value="AE">United Arab Emirates</option>
                        						<option value="GB">United Kingdom</option>
                        						<option value="US">United States</option>
                        						<option value="UM">United States Minor Outlying Islands</option>
                        						<option value="UY">Uruguay</option>
                        						<option value="UZ">Uzbekistan</option>
                        						<option value="VU">Vanuatu</option>
                        						<option value="VE">Venezuela</option>
                        						<option value="VN">Viet Nam</option>
                        						<option value="VG">Virgin Islands (British)</option>
                        						<option value="VI">Virgin Islands (U.S.)</option>
                        						<option value="WF">Wallis and Futuna Islands</option>
                        						<option value="EH">Western Sahara</option>
                        						<option value="YE">Yemen</option>
                        						<option value="YU">Yugoslavia</option>
                        						<option value="ZM">Zambia</option>
                        						<option value="ZW">Zimbabwe</option>
                        					</select>
                                </div>

                                <div class="form-group">
                                    <label for="country-stay">Country of Residence</label>
                                    <select id="country-stay" class="form-control form-input"
                                    placeholder="<?php echo trans("placeholder_country-stay"); ?>"
                                    value="<?php echo old("country-stay"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="country-stay">
                    						<option>Choose Your Country</option>
                    						<option value="AF">Afghanistan</option>
                    						<option value="AL">Albania</option>
                    						<option value="DZ">Algeria</option>
                    						<option value="AS">American Samoa</option>
                    						<option value="AD">Andorra</option>
                    						<option value="AO">Angola</option>
                    						<option value="AI">Anguilla</option>
                    						<option value="AQ">Antarctica</option>
                    						<option value="AG">Antigua and Barbuda</option>
                    						<option value="AR">Argentina</option>
                    						<option value="AM">Armenia</option>
                    						<option value="AW">Aruba</option>
                    						<option value="AU">Australia</option>
                    						<option value="AT">Austria</option>
                    						<option value="AZ">Azerbaijan</option>
                    						<option value="BS">Bahamas</option>
                    						<option value="BH">Bahrain</option>
                    						<option value="BD">Bangladesh</option>
                    						<option value="BB">Barbados</option>
                    						<option value="BY">Belarus</option>
                    						<option value="BE">Belgium</option>
                    						<option value="BZ">Belize</option>
                    						<option value="BJ">Benin</option>
                    						<option value="BM">Bermuda</option>
                    						<option value="BT">Bhutan</option>
                    						<option value="BO">Bolivia</option>
                    						<option value="BA">Bosnia and Herzegowina</option>
                    						<option value="BW">Botswana</option>
                    						<option value="BV">Bouvet Island</option>
                    						<option value="BR">Brazil</option>
                    						<option value="IO">British Indian Ocean Territory</option>
                    						<option value="BN">Brunei Darussalam</option>
                    						<option value="BG">Bulgaria</option>
                    						<option value="BF">Burkina Faso</option>
                    						<option value="BI">Burundi</option>
                    						<option value="KH">Cambodia</option>
                    						<option value="CM">Cameroon</option>
                    						<option value="CA">Canada</option>
                    						<option value="CV">Cape Verde</option>
                    						<option value="KY">Cayman Islands</option>
                    						<option value="CF">Central African Republic</option>
                    						<option value="TD">Chad</option>
                    						<option value="CL">Chile</option>
                    						<option value="CN">China</option>
                    						<option value="CX">Christmas Island</option>
                    						<option value="CC">Cocos (Keeling) Islands</option>
                    						<option value="CO">Colombia</option>
                    						<option value="KM">Comoros</option>
                    						<option value="CG">Congo</option>
                    						<option value="CD">Congo, the Democratic Republic of the</option>
                    						<option value="CK">Cook Islands</option>
                    						<option value="CR">Costa Rica</option>
                    						<option value="CI">Cote d'Ivoire</option>
                    						<option value="HR">Croatia (Hrvatska)</option>
                    						<option value="CU">Cuba</option>
                    						<option value="CY">Cyprus</option>
                    						<option value="CZ">Czech Republic</option>
                    						<option value="DK">Denmark</option>
                    						<option value="DJ">Djibouti</option>
                    						<option value="DM">Dominica</option>
                    						<option value="DO">Dominican Republic</option>
                    						<option value="TP">East Timor</option>
                    						<option value="EC">Ecuador</option>
                    						<option value="EG">Egypt</option>
                    						<option value="SV">El Salvador</option>
                    						<option value="GQ">Equatorial Guinea</option>
                    						<option value="ER">Eritrea</option>
                    						<option value="EE">Estonia</option>
                    						<option value="ET">Ethiopia</option>
                    						<option value="FK">Falkland Islands (Malvinas)</option>
                    						<option value="FO">Faroe Islands</option>
                    						<option value="FJ">Fiji</option>
                    						<option value="FI">Finland</option>
                    						<option value="FR">France</option>
                    						<option value="FX">France, Metropolitan</option>
                    						<option value="GF">French Guiana</option>
                    						<option value="PF">French Polynesia</option>
                    						<option value="TF">French Southern Territories</option>
                    						<option value="GA">Gabon</option>
                    						<option value="GM">Gambia</option>
                    						<option value="GE">Georgia</option>
                    						<option value="DE">Germany</option>
                    						<option value="GH">Ghana</option>
                    						<option value="GI">Gibraltar</option>
                    						<option value="GR">Greece</option>
                    						<option value="GL">Greenland</option>
                    						<option value="GD">Grenada</option>
                    						<option value="GP">Guadeloupe</option>
                    						<option value="GU">Guam</option>
                    						<option value="GT">Guatemala</option>
                    						<option value="GN">Guinea</option>
                    						<option value="GW">Guinea-Bissau</option>
                    						<option value="GY">Guyana</option>
                    						<option value="HT">Haiti</option>
                    						<option value="HM">Heard and Mc Donald Islands</option>
                    						<option value="VA">Holy See (Vatican City State)</option>
                    						<option value="HN">Honduras</option>
                    						<option value="HK">Hong Kong</option>
                    						<option value="HU">Hungary</option>
                    						<option value="IS">Iceland</option>
                    						<option value="IN">India</option>
                    						<option value="ID">Indonesia</option>
                    						<option value="IR">Iran (Islamic Republic of)</option>
                    						<option value="IQ">Iraq</option>
                    						<option value="IE">Ireland</option>
                    						<option value="IL">Israel</option>
                    						<option value="IT">Italy</option>
                    						<option value="JM">Jamaica</option>
                    						<option value="JP">Japan</option>
                    						<option value="JO">Jordan</option>
                    						<option value="KZ">Kazakhstan</option>
                    						<option value="KE">Kenya</option>
                    						<option value="KI">Kiribati</option>
                    						<option value="KP">Korea, Democratic People's Republic of</option>
                    						<option value="KR">Korea, Republic of</option>
                    						<option value="KW">Kuwait</option>
                    						<option value="KG">Kyrgyzstan</option>
                    						<option value="LA">Lao People's Democratic Republic</option>
                    						<option value="LV">Latvia</option>
                    						<option value="LB">Lebanon</option>
                    						<option value="LS">Lesotho</option>
                    						<option value="LR">Liberia</option>
                    						<option value="LY">Libyan Arab Jamahiriya</option>
                    						<option value="LI">Liechtenstein</option>
                    						<option value="LT">Lithuania</option>
                    						<option value="LU">Luxembourg</option>
                    						<option value="MO">Macau</option>
                    						<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
                    						<option value="MG">Madagascar</option>
                    						<option value="MW">Malawi</option>
                    						<option value="MY">Malaysia</option>
                    						<option value="MV">Maldives</option>
                    						<option value="ML">Mali</option>
                    						<option value="MT">Malta</option>
                    						<option value="MH">Marshall Islands</option>
                    						<option value="MQ">Martinique</option>
                    						<option value="MR">Mauritania</option>
                    						<option value="MU">Mauritius</option>
                    						<option value="YT">Mayotte</option>
                    						<option value="MX">Mexico</option>
                    						<option value="FM">Micronesia, Federated States of</option>
                    						<option value="MD">Moldova, Republic of</option>
                    						<option value="MC">Monaco</option>
                    						<option value="MN">Mongolia</option>
                    						<option value="MS">Montserrat</option>
                    						<option value="MA">Morocco</option>
                    						<option value="MZ">Mozambique</option>
                    						<option value="MM">Myanmar</option>
                    						<option value="NA">Namibia</option>
                    						<option value="NR">Nauru</option>
                    						<option value="NP">Nepal</option>
                    						<option value="NL">Netherlands</option>
                    						<option value="AN">Netherlands Antilles</option>
                    						<option value="NC">New Caledonia</option>
                    						<option value="NZ">New Zealand</option>
                    						<option value="NI">Nicaragua</option>
                    						<option value="NE">Niger</option>
                    						<option value="NG">Nigeria</option>
                    						<option value="NU">Niue</option>
                    						<option value="NF">Norfolk Island</option>
                    						<option value="MP">Northern Mariana Islands</option>
                    						<option value="NO">Norway</option>
                    						<option value="OM">Oman</option>
                    						<option value="PK">Pakistan</option>
                    						<option value="PW">Palau</option>
                    						<option value="PA">Panama</option>
                    						<option value="PG">Papua New Guinea</option>
                    						<option value="PY">Paraguay</option>
                    						<option value="PE">Peru</option>
                    						<option value="PH">Philippines</option>
                    						<option value="PN">Pitcairn</option>
                    						<option value="PL">Poland</option>
                    						<option value="PT">Portugal</option>
                    						<option value="PR">Puerto Rico</option>
                    						<option value="QA">Qatar</option>
                    						<option value="RE">Reunion</option>
                    						<option value="RO">Romania</option>
                    						<option value="RU">Russian Federation</option>
                    						<option value="RW">Rwanda</option>
                    						<option value="KN">Saint Kitts and Nevis</option>
                    						<option value="LC">Saint LUCIA</option>
                    						<option value="VC">Saint Vincent and the Grenadines</option>
                    						<option value="WS">Samoa</option>
                    						<option value="SM">San Marino</option>
                    						<option value="ST">Sao Tome and Principe</option>
                    						<option value="SA">Saudi Arabia</option>
                    						<option value="SN">Senegal</option>
                    						<option value="SC">Seychelles</option>
                    						<option value="SL">Sierra Leone</option>
                    						<option value="SG">Singapore</option>
                    						<option value="SK">Slovakia (Slovak Republic)</option>
                    						<option value="SI">Slovenia</option>
                    						<option value="SB">Solomon Islands</option>
                    						<option value="SO">Somalia</option>
                    						<option value="ZA">South Africa</option>
                    						<option value="GS">South Georgia and the South Sandwich Islands</option>
                    						<option value="ES">Spain</option>
                    						<option value="LK">Sri Lanka</option>
                    						<option value="SH">St. Helena</option>
                    						<option value="PM">St. Pierre and Miquelon</option>
                    						<option value="SD">Sudan</option>
                    						<option value="SR">Suriname</option>
                    						<option value="SJ">Svalbard and Jan Mayen Islands</option>
                    						<option value="SZ">Swaziland</option>
                    						<option value="SE">Sweden</option>
                    						<option value="CH">Switzerland</option>
                    						<option value="SY">Syrian Arab Republic</option>
                    						<option value="TW">Taiwan, Province of China</option>
                    						<option value="TJ">Tajikistan</option>
                    						<option value="TZ">Tanzania, United Republic of</option>
                    						<option value="TH">Thailand</option>
                    						<option value="TG">Togo</option>
                    						<option value="TK">Tokelau</option>
                    						<option value="TO">Tonga</option>
                    						<option value="TT">Trinidad and Tobago</option>
                    						<option value="TN">Tunisia</option>
                    						<option value="TR">Turkey</option>
                    						<option value="TM">Turkmenistan</option>
                    						<option value="TC">Turks and Caicos Islands</option>
                    						<option value="TV">Tuvalu</option>
                    						<option value="UG">Uganda</option>
                    						<option value="UA">Ukraine</option>
                    						<option value="AE">United Arab Emirates</option>
                    						<option value="GB">United Kingdom</option>
                    						<option value="US">United States</option>
                    						<option value="UM">United States Minor Outlying Islands</option>
                    						<option value="UY">Uruguay</option>
                    						<option value="UZ">Uzbekistan</option>
                    						<option value="VU">Vanuatu</option>
                    						<option value="VE">Venezuela</option>
                    						<option value="VN">Viet Nam</option>
                    						<option value="VG">Virgin Islands (British)</option>
                    						<option value="VI">Virgin Islands (U.S.)</option>
                    						<option value="WF">Wallis and Futuna Islands</option>
                    						<option value="EH">Western Sahara</option>
                    						<option value="YE">Yemen</option>
                    						<option value="YU">Yugoslavia</option>
                    						<option value="ZM">Zambia</option>
                    						<option value="ZW">Zimbabwe</option>
                    					</select>
                                </div>

                                <div class="form-group">
                                    <label for="state">State of Residence</label>
                                    <input type="text" id="state" class="form-control form-input" placeholder="<?php echo trans("placeholder_state"); ?>"
                                    value="<?php echo old("state"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="state" />
                                </div>

                                <div class="form-group">
                                    <label for="born-again">Are you born again?</label>
                                    <ul>
                						<li><input type="radio" required id="born-again" name="born-again" onclick="whyNot()" value="no" />
                							<label>No</label>
                						</li>
                						<li><input type="radio" required id="born-again" name="born-again" onclick="sinceWhen()" value="yes" />
                							<label>Yes</label>
                						</li>
                					</ul>
                                </div>

                                <div class="form-group" id="why-not" style="display:none">
                                    <label for="why-not">Why not?</label>
                                    <input type="text" class="form-control form-input" placeholder="<?php echo trans("placeholder_why-not"); ?>"
                                    value="<?php echo old("why-not"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="why-not" />
                                </div>

                                <div class="form-group" id="born-again-date" style="display:none">
                                    <label for="born-again-date">Since when?</label>
                                    <input type="date" class="datepicker form-control form-input" placeholder="<?php echo trans("placeholder_born-again-date"); ?>"
                                    value="<?php echo old("born-again-date"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="born-again-date" />
                                </div>

                                <div class="form-group">
                                    <label for="discipleship">Are you in an active Discipleship relationship?</label>
                                    <ul>
                						<li><input type="radio" required id="discipleship" name="discipleship" onclick="whyNoD()" value="no" />
                							<label>No</label>
                						</li>
                						<li><input type="radio" required id="discipleship" name="discipleship" onclick="who()" value="yes" />
                							<label>Yes</label>
                						</li>
                					</ul>
                                </div>

                                <div class="form-group" id="why-no-d" style="display:none">
                                    <label for="why-no-d">Why?</label>
                                    <input type="text" class="form-control form-input" placeholder="<?php echo trans("placeholder_why-no-d"); ?>"
                                    value="<?php echo old("why-no-d"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="why-no-d" />
                                </div>

                                <div class="form-group" id="discipler" style="display:none">
                                    <label for="discipler">Who is your Discipler?</label>
                                    <input type="text" class="form-control form-input" placeholder="<?php echo trans("placeholder_discipler"); ?>"
                                    value="<?php echo old("discipler"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required name="discipler" />
                                </div>

                                <div class="form-group">
                                    <label for="subscribe">Subscribe to get Updates</label>
                                    <ul>
                						<li><input type="radio" id="subscribe-yes" name="subscribe" value="yes" checked="checked" />
                							<label>Of course!</label>
                						</li>
                						<li><input type="radio" id="subscribe-no" name="subscribe" value="no" />
                							<label>Don't bother me!</label>
                						</li>
                					</ul>
                                </div>

                                <div class="form-group">
                                    <label for="find-jegrun">How did you find JEGRUN?</label>
                                    <select size="1" name="find-jegrun" id="find-jegrun" class="form-control form-input" placeholder="<?php echo trans("placeholder_find-jegrun"); ?>"
                                    value="<?php echo old("find-jegrun"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                    					<option value="youtube">Youtube</option>
                    					<option value="facebook">Facebook</option>
                    					<option value="twitter">Twitter</option>
                    					<option value="telegram">Telegram</option>
                    					<option value="a-friend">From a Friend</option>
                    					<option value="peace-house">Peace House</option>
                    					<option value="mixlr">Mixlr</option>
                    					<option value="other">Other</option>
                    				</select>
                                </div>

                                <div class="form-group">
                                    <label for="password">Create an account password</label>
                                    <input type="password" name="password" id="password" class="form-control form-input"
                                           placeholder="<?php echo trans("placeholder_password"); ?>"
                                           value="<?php echo old("password"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="confirm_password" id="confirm-password" class="form-control form-input"
                                           placeholder="<?php echo trans("placeholder_confirm_password"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                                </div>
                                <div class="form-group">
                                    <label class="custom-checkbox">
                                        <input type="checkbox" class="checkbox_terms_conditions" required>
                                        <span class="checkbox-icon"><i class="icon-check"></i></span>
                                        <?php echo trans("terms_conditions_exp"); ?>&nbsp;<a href="<?php echo get_page_link_by_default_name('terms_conditions', $this->selected_lang->id); ?>" class="link-terms" target="_blank"><strong><?php echo trans("terms_conditions"); ?></strong></a>
                                    </label>
                                </div>
                                <?php if ($this->recaptcha_status): ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="recaptcha-cnt">
                                                <?php generate_recaptcha(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="col-sm-12 m-t-15">
                                    <button type="submit" class="btn btn-md btn-custom btn-block margin-top-15">
                                        <?php echo trans("register"); ?>
                                    </button>
                                </div>
                        	<?php echo form_close(); ?>
                            <!-- form end -->
                        </div>
                    </div>
                </div>
            </div>
            <div id="sidebar" class="col-sm-4 col-xs-12">
                <!--include sidebar -->
                <?php $this->load->view('partials/_sidebar'); ?>
            </div>
        </div>
    </div>
</section>
<!-- /.Section: wrapper -->
