<?php defined('BASEPATH') OR exit('No direct script access allowed');?>

<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="Welcome to Jesus Global Revolution">
<meta name="keywords" content="JEGRUN, Jesus Global Revolutio Website">
<meta name="author" content="Obi Madu">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>JEGRUN - Welcome</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="<?php echo base_url(); ?>assets/index/images/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/index/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/index/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/index/images/apple-touch-icon-114x114.png">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/bootstrap.min.css">

<!-- FONT ICONS -->
<!-- IonIcons -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/assets/ionicons/css/ionicons.css">

<!-- Font Awesome 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/assets/font-awesome/css/font-awesome.min.css">
-->

<!-- Elegant Icons -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/assets/elegant-icons/style.css">
<!--[if lte IE 7]><script src="<?php echo base_url(); ?>assets/index/assets/elegant-icons/lte-ie7.js"></script><![endif]-->

<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/owl.theme.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/nivo-lightbox.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/nivo_themes/default/default.css">

<!-- COLOR -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/styles.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/index/css/responsive.css">

<!--[if lt IE 9]>
			<script src="<?php echo base_url(); ?>assets/index/js/html5shiv.js"></script>
			<script src="<?php echo base_url(); ?>assets/index/js/respond.min.js"></script>
<![endif]-->

<!-- ****************
      After neccessary customization/modification, Please minify HTML/CSS according to http://browserdiet.com/en/ for better performance
     **************** -->
     
</head>

<body>
<!-- =========================
     PRE LOADER       
============================== -->
<div class="preloader">
  <div class="status">&nbsp;</div>
</div>

<!-- =========================
	HEADER   
============================== -->
<header id="home">

	<!-- COLOR OVER IMAGE -->
	<div class="color-overlay">
		
		<div class="navigation-header">
			
			<!-- STICKY NAVIGATION -->
			<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
				<div class="container">
					<div class="row">
					<div class="navbar-header">
						
						<!-- LOGO ON STICKY NAV BAR -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#landx-navigation">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img src="<?php echo base_url(); ?>assets/index/images/logo-dark.png" alt=""></a>
						
					</div>
					
					<!-- NAVIGATION LINKS -->
					<div class="navbar-collapse collapse" id="landx-navigation">
						<ul class="nav navbar-nav navbar-right main-navigation">
							<li><a href="#home">Home</a></li>
							<li><a href="#section2">SIGNUP</a></li>
							<li><a href="#section3">MEETINGS</a></li>
							<li><a href="<?php echo base_url(); ?>blog" target="_blank">BLOG</a></li>
							<li><a href="<?php echo base_url(); ?>blog/devotional" target="_blank">DEVOTIONAL</a></li>
							<li><a href="<?php echo base_url(); ?>contact" target="_blank">CONTACT US</a></li>
							<li><a href="<?php echo base_url(); ?>meeting-23-september-2021" class="btn primary-button" style="color: black;" target="_blank" >DOWNLOAD LAST MEETING</a></li>
	  					</ul>
					</div>
				</div>
			</div>
				<!-- /END CONTAINER -->
				
			</div>
			
			<!-- /END STICKY NAVIGATION -->
			
			<!-- ONLY LOGO ON HEADER -->
			<div class="navbar non-sticky">
				
				<div class="container">
					
					<div class="navbar-header">
						<img src="<?php echo base_url(); ?>assets/index/images/logo.png" alt="">
					</div>
					
					<ul class="nav navbar-nav navbar-right social-navigation hidden-xs">
						<li><a href="https://www.youtube.com/channel/UCJ5nIVXWGQVaXZozP-DibRQ"><i class="social_youtube_circle"></i></a></li>
						<li><a href="https://t.me/JEGRUN"><i class="social_telegram_circle"></i></a></li>
						<li><a href="https://www.facebook.com/126920112198844"><i class="social_facebook_circle"></i></a></li>
						<li><a href="https://twitter.com/jegrun"><i class="social_twitter_circle"></i></a></li>
						<li><a href="https://jegrun.com/blog"><i class="social_rss_circle"></i></a></li>
					</ul>
				</div>
				<!-- /END CONTAINER -->
				
			</div>
			<!-- /END ONLY LOGO ON HEADER -->
			
		</div>
		
		<!-- HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
		<!-- =========================
     		SECTION 0 - CAROUSEL
		============================== -->
		
		<div class="row">			
			<div class="col-md-12 col-sm-12">	
				<div id="carouselControls" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="item active">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/JEGRUN BANNER A.jpg" alt="Jegrun slide 1">
						</div>
						<div class="item">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/JEGRUN BANNER B.jpg" alt="Jegrun slide 2">
						</div>
						<div class="item">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/JEGRUN BANNER C.jpg" alt="Jegrun slide 3">
						</div>
						<!-- <div class="item">
							<img class="car" style="height:auto;" src="<?php echo base_url(); ?>assets/index/images/JEGRUN BANNER D.png" alt="Jegrun slide 3">
						</div> -->
					</div>
						<a class="left carousel-control" href="#carouselControls" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carouselControls" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
				</div>
			</div>
	  	</div><!-- /END - CAROUSEL -->

		<!-- /END HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
		
	</div>

</header>

<!-- =========================
     SECTION 1 - INTRO
============================== -->
<section class="section1" id="section1" >
	<div class="container">
			
			<div class="row">
				
				<div class="col-md-6">
				<center>
				<!-- SCREENSHOT -->
				<div class="home-screenshot side-screenshot">
					<img src="<?php echo base_url(); ?>assets/index/images/youthsablaze.jpg" alt="Feature" style="max-width: 100%; max-height: 320px; ">
				</div>
				</center>
				</div>
				
				<!-- RIGHT - HEADING AND TEXTS -->
				<div class="col-md-6" style="color: black;">
					
					<h1 class="intro text-left" style="color: black;">
					<strong>Youths Ablaze</strong> for <span class="strong colored-text">Christ</span>
					</h1>
					
					<p class="sub-heading text-left">
						JEGRUN (Jesus Global Revolution) is a group comprising of young people, whom God has invested in concertedly, 
						set of firebrands who can ignite the fire of Christ on other youths effectively.
					</p>
					
					<!-- CTA BUTTONS -->
					<div id="cta-5" class="button-container">
					
					<a href="<?php echo base_url(); ?>" class="btn standard-button pull-left" target="_blank">Know Us Better</a>
					<a href="<?php echo base_url(); ?>blog" class="btn secondary-button-white pull-left" style="color: black;" target="_blank" >Visit Our Blog</a>
					
					</div>
					
				</div>

			</div>
			
		</div>
</section>

<!-- =========================
     SECTION 2 - SIGNUP
============================== -->
<section class="cta-section2" id="section2">

	<div class="color-overlay">

	<div class="container">
		<div class="row">
			
			<div class="row">
				
				<div class="col-md-6 col-sm-6" style="margin-top: 20px;">
					
					<div class="vertical-registration-form">
						<div class="colored-line">
						</div>
						<h2>SIGNUP NOW!</h2>
							<!-- form start -->
							<?php echo form_open("register-post", ['id' => 'register', 'class' => 'validate_terms registration-form', 'role' => 'form']); ?>

							<!-- include message block -->
							<?php $this->load->view('partials/_messages'); ?>

							<div class="form-group">
								<input type="text" name="username" class="form-control input-box"
									placeholder="Username (Min of 4 char)"
									value="<?php echo old("username"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
							</div>

							<div class="form-group">
								<input type="text" name="first-name" class="form-control input-box"
									placeholder="First Name"
									value="<?php echo old("First_Name"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
							</div>

							<div class="form-group">
								<input type="text" name="last-name" class="form-control input-box"
									placeholder="Last Name"
									value="<?php echo old("Last_Name"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
							</div>

							<div class="form-group">
                                    <input type="number" name="age" class="form-control input-box"
                                       placeholder="Age"
                                       value="<?php echo old("Age"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                            </div>

							<div class="form-group">
									<select size="1" name="gender" id="gender" class="form-control input-box" required>
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
							</div>

							<div class="form-group">
								<input type="email" name="email" class="form-control input-box"
									placeholder="<?php echo trans("placeholder_email"); ?>"
									value="<?php echo old("email"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
							</div>

							<div class="form-group">
                                <input type="text" name="mobile-number" class="form-control input-box"
									placeholder="Mobile Number"
									value="<?php echo old("Mobile_Number"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
                            </div>

							<div class="form-group">
								<input type="password" name="password" class="form-control input-box"
									placeholder="Create a password (Min of 4 char)"
									value="<?php echo old("password"); ?>" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
							</div>
							<div class="form-group">
								<input type="password" name="confirm_password" class="form-control input-box"
									placeholder="Repeat password" <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> required>
							</div>

							<input type="hidden" name="subscribe" value="yes" id="subscribe" >

							<?php if ($this->recaptcha_status): ?>
								<div class="row">
									<div class="col-sm-12">
										<div class="recaptcha-cnt">
											<?php generate_recaptcha(); ?>
										</div>
									</div>
								</div>
							<?php endif; ?>
							<div class="row">
								<div class="col-sm-12 m-t-15">
									<button type="submit" class="btn standard-button">
										SIGNUP
									</button>
									<a href="<?php //check if logged in
        										if (auth_check()) {echo base_url() . 'blog'; } else echo base_url() . 'login'; ?>" class="btn secondary-button" style="color: black;" target="_blank" >SIGN IN</a>
								</div>
							</div>
							<?php echo form_close(); ?><!-- form end -->
					</div>
				</div>
				<!-- /END - REGISTRATION FORM -->
			
			<div class="col-md-6" style="margin-top: 7em;">
				
				<!-- DETAILS WITH LIST -->
				<div class="brief text-left">
					
					<!-- HEADING -->
					<!-- <div class="colored-line pull-right">
					</div> -->
					
					<!-- TEXT -->
					<h4>
						You GET all this by Simply Signing up!
					</h4>
					
					<!-- FEATURE LIST -->
					<ul class="feature-list-2">
						
						<!-- FEATURE -->
						<li>
						<!-- ICON -->
						<div class="icon-container pull-left">
							<span class="icon_check"></span>
						</div>
						<!-- DETAILS -->
						<div class="details pull-left">
							<h6>Specialized Meeting Materials</h6>
							<p>
								We create awesome digestive materials, booklets, ebooks, flipbooks for our meetings. You gain exclussive access to them by signing up.
							</p>
						</div>
						</li>
						
						<!-- FEATURE -->
						<li>
						<!-- ICON -->
						<div class="icon-container pull-left">
							<span class="icon_check"></span>
						</div>
						<!-- DETAILS -->
						<div class="details pull-left">
							<h6>Customized blog Reading Experience</h6>
							<p>
								By signing up you are given a personal account. You can switch the modes of the JEGRUN Blog, change the color Scheme, Follow your favorite JEGRUN Authors, and many more.
							</p>
						</div>
						</li>

						<!-- FEATURE -->
						<li>
							<!-- ICON -->
							<div class="icon-container pull-left">
								<span class="icon_check"></span>
							</div>
							<!-- DETAILS -->
							<div class="details pull-left">
								<h6>Blog Updates</h6>
								<p>
									You get notified in your email the moment a new post lands on our blog. 
								</p>
							</div>
						</li>

						<!-- FEATURE -->
						<li>
							<!-- ICON -->
							<div class="icon-container pull-left">
								<span class="icon_check"></span>
							</div>
							<!-- DETAILS -->
							<div class="details pull-left">
								<h6>Access to our Daily Devotional</h6>
								<p>
									JEGRUN has a wonderful Devotional called "Secret Place". By signing up you get healthy digests and summaries of The Secret Place everyday in your inbox.
								</p>
							</div>
						</li>
					</ul>
				</div>
			</div> <!-- /END DETAILS WITH LIST -->
			
		</div> <!-- END ROW -->
		
	</div> <!-- END CONTAINER -->
	</div>

</section>

<!-- =========================
     SECTION 3 - VIDEOS
============================== -->
<section class="section2" id="section3" style="padding-top: 20px;">

	<div class="container">
		
		<!-- SECTION HEADING -->
		<h2>Watch Our Last Meeting</h2>
		<div class="colored-line">
		</div>
		
		<div class="sub-heading">
			Click below to Watch our last meeting on Youtube.
		</div>
		
		<!-- VIDEO -->
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				
				<div class="video-container">
					
					<!--
					<div class="video">
						
						<iframe src="//player.vimeo.com/video/88902745?byline=0&amp;portrait=0&amp;color=ffffff" width="600" height="338" frameborder="0" allowfullscreen>
						</iframe> 
					</div>
					-->
					
					<div class="video">
						
						<iframe width="640" height="360" src="https://www.youtube.com/embed/EK-0wuAI4-4" frameborder="0" allowfullscreen></iframe>
					
					</div>
					
				</div>
			</div>
		</div>
		
		<!-- FEATURES IN VIDEO SECTION -->
		<div class="row video-features">
			<div class="col-md-10 col-md-offset-1">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<h5>
						<span class="ion-android-contacts colored-text inline-icon"></span> For YOUTHS </h5>
					</div>
					<div class="col-md-3 col-sm-3">
						<h5>
						<span class="ion-android-earth colored-text inline-icon"></span> Worldwide Outreach </h5>
					</div>
					<div class="col-md-3 col-sm-3">
						<h5>
						<span class="ion-android-forums colored-text inline-icon"></span> Ask Questions </h5>
					</div>
					<div class="col-md-3 col-sm-3">
						<h5>
						<span class="ion-android-book colored-text inline-icon"></span> Get Biblical Answers </h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 m-t-15" style="margin-top: 10px; margin-bottom: 10px;">
						<a href="https://www.youtube.com/channel/UCJ5nIVXWGQVaXZozP-DibRQ">
						<button type="submit" class="btn standard-button">
							WATCH ALL MEETINS ON YOUTUBE
						</button>
						</a>
					</div>
					<h5> Visit the Blog's Meeting Section To </h5>
					<div class="col-sm-12 m-t-15" style="margin-top: 10px; margin-bottom: 10px;">
						<a href="<?php echo base_url(); ?>meetings">
						<button type="submit" class="btn standard-button">
							DOWNLOAD MATERIALS
						</button>
						</a>
					</div>					
				</div>
		</div> <!-- /END FEATURES IN VIDEO SECTION -->
		
	</div> <!-- /END CONTAINER -->

</section>

<!-- =========================
     SECTION 4 - CTA  
============================== -->
<section class="cta-section" id="section4">
	<div class="color-overlay">
		
		<div class="container">
			
			<h4>We write a Rich blog!</h4>
			<h2>Articles to feed your soul</h2>
			<div id="cta-4">
				<a href="<?php echo base_url(); ?>blog" class="btn standard-button">Take ME There!!</a>
			</div>
			
			<!-- MAILCHIMP SUBSCRIBE FORM / REMOVE 'hidden' CLASS TO MAKE FORM VISIBLE -->
			
			<div class="subscribe-section">
				
				<h3>Or Subscribe to get them straight in your Inbox!!!</h3>
				
				<?php echo form_open('add-to-newsletter', ["class" => "subscription-form form-inline"]); ?>
                                <div class="newsletter">

									<input type="email" class="form-control input-box" name="email" id="subscriber-email" maxlength="199" placeholder="<?php echo trans("placeholder_email"); ?>" required <?php echo ($this->rtl == true) ? 'dir="rtl"' : ''; ?> > 
									<button type="submit" id="subscribe-button" class="btn standard-button"><?php echo trans("subscribe"); ?></button>
								</div>
                                <p id="newsletter" style="color: white;">
                                    <?php
                                    if ($this->session->flashdata('news_error')):
                                        echo '<strong>'. $this->session->flashdata('news_error') . '</strong>' ;
                                    endif;
                                    if ($this->session->flashdata('news_success')):
                                        echo '<strong>' . $this->session->flashdata('news_success') . '</strong>' ;
                                    endif;
                                    ?>
                                </p>
				<?php echo form_close(); ?>
			</div>
			<!-- /END SUBSCRIPTION FORM -->
			
		</div>  <!-- /END CONTAINER -->
	</div>  <!-- /END COLOR OVERLAY -->

</section>

<!-- =========================
     SECTION 5 - DEVOTIONAL   
============================== -->
<section class="devotional" id="section5">

	<div class="container">
		<div class="row">
			
			<div class="col-md-6">
				
				<!-- SCREENSHOT -->
				<div class="" style="margin-top: 20;">
					<img src="<?php echo base_url(); ?>assets/index/images/secretplace.jpg" alt="Secret Place" style="max-width: 100%; height: 320px;" >
				</div>
				
			</div>
			
			<div class="col-md-6">
				
				<!-- DETAILS WITH LIST -->
				<div class="brief text-left">
					
					<!-- HEADING -->
					<h2>Meet <strong><span class="strong colored-text">Secret Place</span></strong></h2>
					<div class="colored-line pull-left">
					</div>
					
					<!-- TEXT -->
					<p>
						Secret Place is a divinely inspired Devotional from the JEGRUN Team. Every morning it leads you to worship and hear God through different Bible passages, with relevant summaries and illustrations to enhance your understanding.
					</p>
					
					<!-- FEATURE LIST -->
					<ul class="feature-list-2">
						<!-- FEATURE -->
						<li>
						<!-- ICON -->
						<div class="icon-container pull-left">
							<span class="icon_check"></span>
						</div>
						<!-- DETAILS -->
						<div class="details pull-left">
							<h6>Get it NOW</h6>
							<p>
								Click below to download the App on Play Store or to access Daily Excerpts and summaries on our blog.
							</p>
						</div>
						</li>
					</ul>
					<a href="https://play.google.com/store/apps/details?id=com.cpaii.secretplaceversiontwo" class="btn standard-button pull-left" target="_blank">Download App</a>
					<a href="<?php echo base_url(); ?>devotional" class="btn secondary-button pull-left" target="_blank" >Read Summaries</a>
				</div>
			</div> <!-- /END DETAILS WITH LIST -->
			
		</div> <!-- END ROW -->
		
	</div> <!-- END CONTAINER -->
</section>

<!-- =========================
     SECTION 6 - CONTACT US  
============================== -->
<section class="contact-us" id="section6">
	<div class="container">
		
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				
				<h3 class="heading">Need any help? Contact us now!</h3>
				
				<a href="" class="contact-link expand-form"><span class="icon_mail_alt"></span>Contact us now</a>
				
				<!-- EXPANDED CONTACT FORM -->
				<div class="expanded-contact-form">
					
					<!-- FORM -->
					<form class="contact-form" id="contact" role="form">
						
						<!-- IF MAIL SENT SUCCESSFULLY -->
						<h6 class="success">
						<span class="olored-text icon_check"></span> Your message has been sent successfully. </h6>
						
						<!-- IF MAIL SENDING UNSUCCESSFULL -->
						<h6 class="error">
						<span class="colored-text icon_error-circle_alt"></span> E-mail must be valid and message must be longer than 1 character. </h6>
						
						<div class="field-wrapper col-md-6">
							<input class="form-control input-box" id="cf-name" type="text" name="cf-name" placeholder="Your Name">
						</div>
						
						<div class="field-wrapper col-md-6">
							<input class="form-control input-box" id="cf-email" type="email" name="cf-email" placeholder="Email">
						</div>
						
						<div class="field-wrapper col-md-12">
							<input class="form-control input-box" id="cf-subject" type="text" name="cf-subject" placeholder="Subject">
						</div>
						
						<div class="field-wrapper col-md-12">
							<textarea class="form-control textarea-box" id="cf-message" rows="7" name="cf-message" placeholder="Your Message"></textarea>
						</div>
						
						<button class="btn standard-button" type="submit" id="cf-submit" name="submit" data-style="expand-left">Send Message</button>
					</form>
					<!-- /END FORM -->
				</div>
				
			</div>
		</div>
		
	</div>

</section>

<!-- =========================
     SECTION 7 - FOOTER 
============================== -->
<footer class="bgcolor-2">
	<div class="container">
		
		<div class="footer-logo">
			<img src="<?php echo base_url(); ?>assets/index/images/logo-dark.png" alt="">
		</div>
		
		<div class="copyright">
			©2021 Jesus Global Revolution.
		</div>
		
		<ul class="social-icons">
			<li><a href="https://www.youtube.com/channel/UCJ5nIVXWGQVaXZozP-DibRQ"><span class="social_youtube_square"></span></a></li>
			<li><a href="https://twitter.com/jegrun"><span class="social_twitter_square"></span></a></li>
			<li><a href="https://jegrun.com/blog"><span class="social_rss_square"></span></a></li>
			<li><a href="https://www.facebook.com/126920112198844"><span class="social_facebook_square"></span></a></li>
		</ul>
		
	</div>
</footer>


<!-- =========================
     SCRIPTS   
============================== -->
<script src="<?php echo base_url(); ?>assets/index/js/jquery.min.js"></script>

<script>
    $(document).ready(function(){$("#featured-slider").slick({autoplay:true,autoplaySpeed:4900,slidesToShow:1,slidesToScroll:1,infinite:true,speed:200,rtl:rtl,swipeToSlide:true,cssEase:"linear",lazyLoad:"progressive",prevArrow:$("#featured-slider-nav .prev"),nextArrow:$("#featured-slider-nav .next"),});$("#random-slider").slick({autoplay:true,autoplaySpeed:4900,slidesToShow:1,slidesToScroll:1,infinite:true,speed:200,rtl:rtl,lazyLoad:"progressive",prevArrow:$("#random-slider-nav .prev"),nextArrow:$("#random-slider-nav .next"),});$("#post-detail-slider").slick({autoplay:false,autoplaySpeed:4900,slidesToShow:1,slidesToScroll:1,infinite:false,speed:200,rtl:rtl,adaptiveHeight:true,lazyLoad:"progressive",prevArrow:$("#post-detail-slider-nav .prev"),nextArrow:$("#post-detail-slider-nav .next"),});$(".main-menu .dropdown").hover(function(){$(".li-sub-category").removeClass("active");$(".sub-menu-inner").removeClass("active");$(".sub-menu-right .filter-all").addClass("active")},function(){});$(".main-menu .navbar-nav .dropdown-menu").hover(function(){var b=$(this).attr("data-mega-ul");if(b!=undefined){$(".main-menu .navbar-nav .dropdown").removeClass("active");$(".mega-li-"+b).addClass("active")}},function(){$(".main-menu .navbar-nav .dropdown").removeClass("active")});$(".li-sub-category").hover(function(){var b=$(this).attr("data-category-filter");$(".li-sub-category").removeClass("active");$(this).addClass("active");$(".sub-menu-right .sub-menu-inner").removeClass("active");$(".sub-menu-right .filter-"+b).addClass("active")},function(){});$(".news-ticker ul li").delay(500).fadeIn(100);$(".newsticker").newsTicker({row_height:30,max_rows:1,speed:400,direction:"up",duration:4000,autostart:1,pauseOnHover:0,prevButton:$("#btn_newsticker_prev"),nextButton:$("#btn_newsticker_next")});$(window).scroll(function(){if($(this).scrollTop()>100){$(".scrollup").fadeIn()}else{$(".scrollup").fadeOut()}});$(".scrollup").click(function(){$("html, body").animate({scrollTop:0},700);return false});$("form").submit(function(){$("input[name='"+csfr_token_name+"']").val($.cookie(csfr_cookie_name))});$(document).ready(function(){$('[data-toggle-tool="tooltip"]').tooltip()})});$("#form_validate").validate();$("#search_validate").validate();$(document).on("click",".btn-open-mobile-nav",function(){if($("#navMobile").hasClass("nav-mobile-open")){$("#navMobile").removeClass("nav-mobile-open");$("#overlay_bg").hide()}else{$("#navMobile").addClass("nav-mobile-open");$("#overlay_bg").show()}});$(document).on("click","#overlay_bg",function(){$("#navMobile").removeClass("nav-mobile-open");$("#overlay_bg").hide()});$(".close-menu-click").click(function(){$("#navMobile").removeClass("nav-mobile-open");$("#overlay_bg").hide()});$(window).on("load",function(){$(".show-on-page-load").css("visibility","visible")});$(document).ready(function(){$("iframe").attr("allowfullscreen","")});var custom_scrollbar=$(".custom-scrollbar");if(custom_scrollbar.length){var ps=new PerfectScrollbar(".custom-scrollbar",{wheelPropagation:true,suppressScrollX:true})}var custom_scrollbar=$(".custom-scrollbar-followers");if(custom_scrollbar.length){var ps=new PerfectScrollbar(".custom-scrollbar-followers",{wheelPropagation:true,suppressScrollX:true})}$(".search-icon").click(function(){if($(".search-form").hasClass("open")){$(".search-form").removeClass("open");$(".search-icon i").removeClass("icon-times");$(".search-icon i").addClass("icon-search")}else{$(".search-form").addClass("open");$(".search-icon i").removeClass("icon-search");$(".search-icon i").addClass("icon-times")}});$(document).ready(function(){$("#form-login").submit(function(a){a.preventDefault();var b=$(this);var c=b.serializeArray();c.push({name:csfr_token_name,value:$.cookie(csfr_cookie_name)});$.ajax({url:base_url+"auth_controller/login_post",type:"post",data:c,success:function(e){var d=JSON.parse(e);if(d.result==1){location.reload()}else{if(d.result==0){document.getElementById("result-login").innerHTML=d.error_message}}}})})});function add_reaction(b,c){var a={post_id:b,reaction:c,sys_lang_id:sys_lang_id};a[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({method:"POST",url:base_url+"home_controller/save_reaction",data:a}).done(function(d){document.getElementById("reactions_result").innerHTML=d})}$(document).ready(function(){$("#make_comment_registered").submit(function(b){b.preventDefault();var c=$(this).serializeArray();var a={sys_lang_id:sys_lang_id};var d=true;$(c).each(function(f,e){if($.trim(e.value).length<1){$("#make_comment_registered [name='"+e.name+"']").addClass("is-invalid");d=false}else{$("#make_comment_registered [name='"+e.name+"']").removeClass("is-invalid");a[e.name]=e.value}});a.limit=$("#post_comment_limit").val();a[csfr_token_name]=$.cookie(csfr_cookie_name);if(d==true){$.ajax({type:"POST",url:base_url+"home_controller/add_comment_post",data:a,success:function(f){var e=JSON.parse(f);if(e.type=="message"){document.getElementById("message-comment-result").innerHTML=e.message}else{document.getElementById("comment-result").innerHTML=e.message}$("#make_comment_registered")[0].reset()}})}});$("#make_comment").submit(function(b){b.preventDefault();var c=$(this).serializeArray();var a={sys_lang_id:sys_lang_id};var d=true;$(c).each(function(f,e){if($.trim(e.value).length<1){$("#make_comment [name='"+e.name+"']").addClass("is-invalid");d=false}else{$("#make_comment [name='"+e.name+"']").removeClass("is-invalid");a[e.name]=e.value}});a.limit=$("#post_comment_limit").val();a[csfr_token_name]=$.cookie(csfr_cookie_name);if(is_recaptcha_enabled==true){if(typeof a["g-recaptcha-response"]==="undefined"){$(".g-recaptcha").addClass("is-recaptcha-invalid");d=false}else{$(".g-recaptcha").removeClass("is-recaptcha-invalid")}}if(d==true){$(".g-recaptcha").removeClass("is-recaptcha-invalid");$.ajax({type:"POST",url:base_url+"home_controller/add_comment_post",data:a,success:function(f){var e=JSON.parse(f);if(e.type=="message"){document.getElementById("message-comment-result").innerHTML=e.message}else{document.getElementById("comment-result").innerHTML=e.message}if(is_recaptcha_enabled==true){grecaptcha.reset()}$("#make_comment")[0].reset()}})}})});$(document).on("click",".btn-subcomment-registered",function(){var a=$(this).attr("data-comment-id");var b={sys_lang_id:sys_lang_id};b[csfr_token_name]=$.cookie(csfr_cookie_name);$("#make_subcomment_registered_"+a).ajaxSubmit({beforeSubmit:function(){var d=$("#make_subcomment_registered_"+a).serializeArray();var c=$.trim(d[0].value);if(c.length<1){$(".form-comment-text").addClass("is-invalid");return false}else{$(".form-comment-text").removeClass("is-invalid")}},type:"POST",url:base_url+"home_controller/add_comment_post",data:b,success:function(d){var c=JSON.parse(d);if(c.type=="message"){document.getElementById("message-subcomment-result-"+a).innerHTML=c.message}else{document.getElementById("comment-result").innerHTML=c.message}$(".visible-sub-comment form").empty()}})});$(document).on("click",".btn-subcomment",function(){var a=$(this).attr("data-comment-id");var b={sys_lang_id:sys_lang_id};b[csfr_token_name]=$.cookie(csfr_cookie_name);b.limit=$("#post_comment_limit").val();var c="#make_subcomment_"+a;$(c).ajaxSubmit({beforeSubmit:function(){var d=$("#make_subcomment_"+a).serializeArray();var e=true;$(d).each(function(g,f){if($.trim(f.value).length<1){$(c+" [name='"+f.name+"']").addClass("is-invalid");e=false}else{$(c+" [name='"+f.name+"']").removeClass("is-invalid");b[f.name]=f.value}});if(is_recaptcha_enabled==true){if(typeof b["g-recaptcha-response"]==="undefined"){$(c+" .g-recaptcha").addClass("is-recaptcha-invalid");e=false}else{$(c+" .g-recaptcha").removeClass("is-recaptcha-invalid")}}if(e==false){return false}},type:"POST",url:base_url+"home_controller/add_comment_post",data:b,success:function(e){if(is_recaptcha_enabled==true){grecaptcha.reset()}var d=JSON.parse(e);if(d.type=="message"){document.getElementById("message-subcomment-result-"+a).innerHTML=d.message}else{document.getElementById("comment-result").innerHTML=d.message}$(".visible-sub-comment form").empty()}})});function load_more_comment(c){var b=parseInt($("#post_comment_limit").val());var a={post_id:c,limit:b,sys_lang_id:sys_lang_id};a[csfr_token_name]=$.cookie(csfr_cookie_name);$("#load_comment_spinner").show();$.ajax({type:"POST",url:base_url+"home_controller/load_more_comment",data:a,success:function(d){setTimeout(function(){$("#load_comment_spinner").hide();var e=JSON.parse(d);if(e.result==1){document.getElementById("comment-result").innerHTML=e.html_content}},1000)}})}function delete_comment(a,c,b){swal({text:b,icon:"warning",buttons:[sweetalert_cancel,sweetalert_ok],dangerMode:true,}).then(function(f){if(f){var e=parseInt($("#post_comment_limit").val());var d={id:a,post_id:c,limit:e,sys_lang_id:sys_lang_id};d[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"home_controller/delete_comment_post",data:d,success:function(h){var g=JSON.parse(h);if(g.result==1){document.getElementById("comment-result").innerHTML=g.html_content}}})}})}function show_comment_box(a){if($("#sub_comment_form_"+a).html().length>0){$("#sub_comment_form_"+a).empty()}else{$(".visible-sub-comment").empty();var c=parseInt($("#post_comment_limit").val());var b={comment_id:a,limit:c,sys_lang_id:sys_lang_id};b[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"home_controller/load_subcomment_box",data:b,success:function(d){$("#sub_comment_form_"+a).append(d)}})}}$(document).on("click",".btn-comment-like",function(){if($(this).hasClass("comment-liked")){$(this).removeClass("comment-liked")}else{$(this).addClass("comment-liked")}var a=$(this).attr("data-comment-id");var b={comment_id:a,sys_lang_id:sys_lang_id};b[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"home_controller/like_comment_post",data:b,success:function(d){var c=JSON.parse(d);if(c.result==1){document.getElementById("lbl_comment_like_count_"+a).innerHTML=c.like_count}}})});function view_poll_results(b){$("#poll_"+b+" .question").hide();$("#poll_"+b+" .result").show()}function view_poll_options(b){$("#poll_"+b+" .result").hide();$("#poll_"+b+" .question").show()}$(document).ready(function(){var b;$(".poll-form").submit(function(h){h.preventDefault();if(b){b.abort()}var a=$(this);var g=a.find("input, select, button, textarea");var j=a.serializeArray();j.push({name:csfr_token_name,value:$.cookie(csfr_cookie_name)});var i=$(this).attr("data-form-id");b=$.ajax({url:base_url+"home_controller/add_vote",type:"post",data:j,});b.done(function(c){g.prop("disabled",false);if(c=="required"){$("#poll-required-message-"+i).show();$("#poll-error-message-"+i).hide()}else{if(c=="voted"){$("#poll-required-message-"+i).hide();$("#poll-error-message-"+i).show()}else{document.getElementById("poll-results-"+i).innerHTML=c;$("#poll_"+i+" .result").show();$("#poll_"+i+" .question").hide()}}})})});function add_delete_from_reading_list(b){$(".tooltip").hide();var a={post_id:b,};a[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"ajax_controller/add_delete_reading_list_post",data:a,success:function(c){location.reload()}})}function load_more_posts(){$(".btn-load-more").prop("disabled",true);$("#load_posts_spinner").show();var c=parseInt($("#load_more_posts_last_id").val());var b=parseInt($("#load_more_posts_lang_id").val());var a={load_more_posts_last_id:c,load_more_posts_lang_id:b};a[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"home_controller/load_more_posts",data:a,success:function(e){var d=JSON.parse(e);if(d.result==1){setTimeout(function(){$("#last_posts_content").append(d.html_content);$("#load_more_posts_last_id").val(d.last_id);$("#load_posts_spinner").hide();$(".btn-load-more").prop("disabled",false);if(d.hide_button){$(".btn-load-more").hide()}},300)}else{setTimeout(function(){$("#load_more_posts_last_id").val(d.last_id);$("#load_posts_spinner").hide();$(".btn-load-more").hide()},300)}}})}function load_more_comments(f){var e=parseInt($("#vr_comment_limit").val());var d={post_id:f,limit:e,};d[csfr_token_name]=$.cookie(csfr_cookie_name);$("#load_comments_spinner").show();$.ajax({method:"POST",url:base_url+"home_controller/load_more_comments",data:d}).done(function(a){setTimeout(function(){$("#load_comments_spinner").hide();$("#vr_comment_limit").val(e+5);document.getElementById("comment-result").innerHTML=a},500)})}$(document).on("click",".widget-popular-posts .btn-nav-tab",function(){var b=$(this).attr("data-date-type");var c=$(this).attr("data-lang-id");var a={date_type:b,lang_id:c};a[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"ajax_controller/get_popular_posts",data:a,success:function(e){var d=JSON.parse(e);if(d.result==1){setTimeout(function(){document.getElementById("tab_popular_posts_response").innerHTML=d.html_content},100)}}})});$(document).on("click",".visual-color-box",function(){var a=$(this).attr("data-color");$(".visual-color-box").empty();$(this).html('<i class="icon-check"></i>');$("#input_user_site_color").val(a)});function hide_cookies_warning(){$(".cookies-warning").hide();var a={};a[csfr_token_name]=$.cookie(csfr_cookie_name);$.ajax({type:"POST",url:base_url+"home_controller/cookies_warning",data:a,success:function(b){}})}$("#print_post").on("click",function(){$(".post-content .title, .post-content .post-meta, .post-content .post-image, .post-content .post-text").printThis({importCSS:true,})});$(document).ajaxStop(function(){function d(a){$("#poll_"+a+" .question").hide();$("#poll_"+a+" .result").show()}function c(a){$("#poll_"+a+" .result").hide();$("#poll_"+a+" .question").show()}});$(document).ready(function(){$(".validate_terms").submit(function(a){if(!$(".checkbox_terms_conditions").is(":checked")){a.preventDefault();$(".custom-checkbox .checkbox-icon").addClass("is-invalid")}else{$(".custom-checkbox .checkbox-icon").removeClass("is-invalid")}})});$(document).ready(function(){$(".gallery-post-buttons a").css("opacity","1")});$(document).ready(function(b){b(".image-popup-single").magnificPopup({type:"image",titleSrc:function(a){return a.el.attr("title")+"<small></small>"},image:{verticalFit:true,},gallery:{enabled:false,navigateByImgClick:true,preload:[0,1]},removalDelay:100,fixedContentPos:true,})});$(document).on("click","#btn_subscribe_footer",function(){var a=$("#newsletter_email_footer").val();$("#newsletter_email_modal").val(a);$("#modal_newsletter").modal("show")});
</script>

<script>
/* =================================
   LOADER                     
=================================== */
// makes sure the whole site is loaded
jQuery(window).load(function() {
	"use strict";
        // will first fade out the loading animation
	jQuery(".status").fadeOut();
        // will fade out the whole DIV that covers the website.
	jQuery(".preloader").delay(1000).fadeOut("slow");
})

</script>

<script src="<?php echo base_url(); ?>assets/index/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/retina-1.1.0.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/smoothscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/jquery.localScroll.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/nivo-lightbox.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/simple-expand.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/jquery.nav.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/jquery.fitvids.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/jquery.ajaxchimp.min.js"></script>
<script src="<?php echo base_url(); ?>assets/index/js/custom.js"></script>



<a href="#" class="scrollup"><i class="icon-arrow-up"></i></a>
<?php echo $this->general_settings->google_analytics; ?>
<?php echo $this->general_settings->custom_javascript_codes; ?>


</body>
</html>

</body>
</html>